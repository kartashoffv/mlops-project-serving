import os
import pickle
import pandas as pd
from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session
from db import models, database
from pydantic import BaseModel
from .auth import get_current_user
from .billing import deduct_balance


router = APIRouter()

UPLOAD_DIR = "./data/user/uploaded_files"
DOWNLOADED_DIR = "./data/user/downloaded_files"
MODEL_DIR = "./models"


class RunModelRequest(BaseModel):
    data_id: str
    model_name: str
    model_path: str
    model_cost: int


def run_model(data, file_path):
    with open(file_path, "rb") as file:
        model = pickle.load(file)
    predictions = model.predict(data)

    return predictions


@router.post("/run-model")
async def execute_model(
    request: RunModelRequest,
    db: Session = Depends(database.get_db),
    current_user: models.User = Depends(get_current_user),
):
    data_file_path = os.path.join(UPLOAD_DIR, f"{request.data_id}.csv")
    model_file_path = request.model_path

    if not os.path.exists(data_file_path):
        raise HTTPException(status_code=404, detail="Uploaded data not found")

    if not os.path.exists(model_file_path):
        raise HTTPException(status_code=404, detail="Model file not found")

    try:
        data = pd.read_csv(data_file_path, sep=";")
        predictions = run_model(data, model_file_path)
        deduct_balance(current_user.id, request.model_cost, db)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

    output = pd.DataFrame(predictions, columns=["Status"])
    file_path = f"prediction_{request.data_id}.csv"
    download_path = os.path.join(
        "./data/user/downloaded_files", f"prediction_{request.data_id}.csv"
    )
    output.to_csv(download_path)

    return {
        "model_name": request.model_name,
        "price": request.model_cost,
        "predictions": predictions.tolist(),
        "file_path": file_path,
    }


@router.get("/download-files/{file_path}")
async def download(file_path: str):
    absolute_file_path = os.path.join(DOWNLOADED_DIR, file_path)

    if os.path.exists(absolute_file_path):
        return FileResponse(
            path=absolute_file_path,
            filename=file_path,
            media_type="application/octet-stream",
        )
    else:
        raise HTTPException(status_code=404, detail="File not found")
