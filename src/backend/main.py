from fastapi import FastAPI
from fastapi.middleware.wsgi import WSGIMiddleware

from endpoints.auth import router as auth_router
from endpoints.billing import router as billing_router
from endpoints.data_management import router as data_management_router
from endpoints.model_execution import router as model_execution_router
from endpoints.model_broker import router as model_broker_router

from db.database import SessionLocal, engine
from db.models import MLModel, Base

from frontend import app as dash_app
# dash_app
# import app as dash_app

app = FastAPI()

# Include the routers
app.include_router(auth_router)
app.include_router(billing_router)
app.include_router(data_management_router)
app.include_router(model_execution_router)
app.include_router(model_broker_router)

app.mount("/dash", WSGIMiddleware(dash_app.server))


def init_db():
    Base.metadata.create_all(bind=engine)
    db = SessionLocal()

    # Check if the data already exists to prevent duplicates.
    if db.query(MLModel).first() is None:
        new_model = MLModel(name="CatBoost", file_path="./models/CatBoost.pkl", cost=30)
        new_model1 = MLModel(
            name="Logistic Regression",
            file_path="./models/LogisticRegression.pkl",
            cost=10,
        )
        new_model2 = MLModel(
            name="Random Forest", file_path="./models/RandomForest.pkl", cost=20
        )

        db.add(new_model)
        db.add(new_model1)
        db.add(new_model2)
        db.commit()

    db.close()


@app.on_event("startup")
async def startup_event():
    init_db()


if __name__ == "__main__":
    # from db.database import engine
    import db.models as models

    models.Base.metadata.create_all(bind=engine)
    import uvicorn

    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
